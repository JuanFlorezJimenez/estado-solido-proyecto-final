#include <cmath>
#include <iostream>
#include "Random64.h"

using namespace std;

const int L=8;
const int L2=L*L;

//--------------------Class Ising -------------------------------------
class SpinSystem{
	
	private:
		int s[L][L], E, M;
 		
	public:
	void inicie(void);
	void UnPasoDeMetropolis(double beta, Crandom & ran64);
	double GetE(void){return (double) E;};
	double GetM(void){return fabs(M);};
};

void SpinSystem::inicie(void)
{
	for(int i=0; i<L; i++)
		for(int j=0; j<L; j++)
			s[i][j]=1;
	E=2*L2; M=L2;
}

void SpinSystem::UnPasoDeMetropolis(double beta, Crandom & ran64){
	int n,i,j;
	double dE;
	//Escoger un spin al azar
	n=(int)(L2*ran64.r());
	i = n/L; j=n%L;
	//Calcular el dE que se produciria si yo volteo el espin
	dE = 2*s[i][j]*(s[i][(j-1+L)%L]+s[i][(j+1)%L]+s[(i-1+L)%L][j]+s[(i+1)%L][j]);
	if(dE<=0)
		{s[i][j]*=-1;	E+=dE;		M+=2*s[i][j];}
	else if(ran64.r() < exp(-beta*dE))
		{s[i][j]*=-1;	E+=dE;		M+=2*s[i][j];}
}

const int teq=(int)(200*pow(L/8.0,2.125));
const int tcorr=(int)(50*pow(L/8.0,2.125));
const int Nmuestras=10000;


int main(void){
	SpinSystem Ising;
	Crandom ran64(1);
	int t, mcs, cmuestras;
	double Mprom, M;
	
	double KT, beta;
	
	for(KT=0.2;KT<4;KT+=0.1){
	beta=1.0/KT;
	Mprom = 0;
	
	Ising.inicie();

	for(t=0; t<teq; t++)
		for(mcs=0; mcs<L2; mcs++)
			Ising.UnPasoDeMetropolis(beta, ran64);

	for(cmuestras=0; cmuestras<Nmuestras; cmuestras++)
	{
		M=Ising.GetM();
		Mprom += M;
		for(t=0; t<tcorr; t++)
			for(mcs=0; mcs<L2; mcs++)
				Ising.UnPasoDeMetropolis(beta, ran64);
	}
	
	Mprom /= Nmuestras;
	cout << KT << " " << Mprom << endl;		

	}

	return 0;
	
	}
