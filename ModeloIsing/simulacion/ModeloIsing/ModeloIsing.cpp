#include <cmath>
#include <iostream>
#include "Random64.h"
using namespace std;

const int L=8;
const int L2 = L*L;
//Constantes que permiten
const int teq=(int)(200*pow(L/8.0,2.125));
const int tcor=(int)(50*pow(L/8.0,2.125));
const int Nmuestra=10000;

//_____________Clase Ising__________

class SpinSystem{
private:
  int s[L][L],E,M; 

public:  
  void Inicie(int ii, Crandom & ran64);
  double GetE(void){return (double)E;};
  double GetM(void){return fabs(M);};
  void PasoMetropilis(double Beta, Crandom & ran64);
};


void SpinSystem::Inicie(int ii,Crandon & ran64){
  if(ii==1){//T=0
  for(int ii=0;ii<L;ii++)
      for(int jj=0;jj<L;jj++)
	s[ii][jj]=1;
 E=2*L2; M=L2;
  }
  else{//Condiciones iniciales T=inf
    for(int ii=0;ii<L;ii++){
      for(int jj=0;jj<L;jj++){
	s[ii][jj]=1;
      }
    }

    E=2*L2; M=L2;

    int n,i,j;

    for(ii=0;ii<L2;ii++){//Cambiar aleatoriamente n spines
       n=(int)(L2*ran64.r());
       i = n/L; j=n%L;
       s[i][j]*=-1; E+=dE; M+=2*s[i][j];//Se registra el cambio de las variables del sistema
     }
	
       
     }

  }
  


void SpinSystem::PasoMetropilis(double Beta, Crandom & ran64){
	int n,i,j;
	double dE;
	//Escoger un spin al azar
	n=(int)(L2*ran64.r());
	i = n/L; j=n%L;
	//Calcular el dE que se produciria si yo volteo el espin
	dE = 2*s[i][j]*(s[i][(j-1+L)%L]+s[i][(j+1)%L]+s[(i-1+L)%L][j]+s[(i+1)%L][j]);
	if(dE<=0)
		{s[i][j]*=-1;	E+=dE;		M+=2*s[i][j];}
	else if(ran64.r() < exp(-Beta*dE))
		{s[i][j]*=-1;	E+=dE;		M+=2*s[i][j];}
}



int main(void){

  SpinSystem Ising;
  Crandom ran64(0);
  int t,mcs,cmuestras;
  double Mprom,M;
  double KT,beta;

  KT=2.2;
  beta=1.0/KT;
  
  //iniciar los acumuladores

  Mprom=0;

  //Inicio y equilibrio del sistema

  Ising.Inicie();

  for(t=0;t<teq;t++)
    for(mcs=0;mcs<L2;mcs++)
      Ising.PasoMetropilis(beta,ran64);
  

    //Tomar datos y evolucionar
    for(cmuestras=0;cmuestras<Nmuestra;cmuestras++){
      //tomar datos
      M=Ising.GetM();
      Mprom+=M;
      //Evolucionar hasta la siguiente muestra: tcor pasos de Monte Carlo
      for(t=0;t<tcor;t++)
	for(mcs=0;mcs<L2;mcs++)
	  Ising.PasoMetropilis(beta,ran64);
      }

      //Procesar los datos
    Mprom /= Nmuestra;   


	
      //Imprima
 
    cout<<KT<<" "<<Mprom<<endl;
	/*
 
  return 0;
}
