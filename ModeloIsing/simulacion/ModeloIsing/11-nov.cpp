#include <cmath>
#include <iostream>
#include "Random64.h"
using namespace std;

const int L=10;
const int L2=L*L;
const double kB=1;

// Class Spin System

class SpinSystem{

private:

	int s[L][L],E,M;
public:

	void Inicie(void);
	void UnPasoDeMetropolis(double Beta,Crandom & ran64);
	double GetE(void){return (double) E;};
	double GetM(void){return fabs(M);};

};

void SpinSystem::Inicie(void){
	for(int i=0;i<L;i++)
		for(int j=0;j<L;j++)
			s[i][j]=1;
			E=-2*L2; M=L2;

}

void SpinSystem::UnPasoDeMetropolis(double Beta,Crandom & ran64){
	
	int n,i,j,dE;
		
	//Escoger un espin al azar;
	n=(int)(L2*ran64.r());
	i=n/L;
	j=n%L;
	//Calcular el dE que se produciria si yo cambio el spin;
	dE=2*s[i][j]*(s[i][(j-1+L)%L]+s[i][(j+1)%L]+s[(i-1+L)%L][j]+s[(i+1)%L][j]);
	
	if(dE<=0)
		{s[i][j]*=-1; E+=dE; M+=2*s[i][j];} //lo volteo;
	else if(ran64.r()<exp(-Beta*dE))
		{s[i][j]*=-1; E+=dE; M+=2*s[i][j];} //lo volteo;

}

const int teq = (int)(200*pow(L/8.0,2.125));
const int tcorr = (int)(50*pow(L/8.0,2.125));
const int Nmuestras = 10000;

int main(void){
	SpinSystem Ising;
	Crandom ran64(1);
	int t,mcs,cmuestras;

	double E,M;
	double Mprom,M2prom,M4prom,Eprom,E2prom;
	double Cv,Xs,Ub;
	
	double kT, Beta;

	for(kT=0.2; kT<5.0;kT+=0.1){

	Beta = 1.0/kT;
	

	// Inicio los acumuladores en ceo
	Mprom=M2prom=M4prom=Eprom=E2prom=0;
	// Inicio y equilibrio el sistema 
	Ising.Inicie(); //Inicio con todos los espines hacia ariba
	
	for(t=0;t<teq;t++)
		for(mcs=0;mcs<L2;mcs++)
			Ising.UnPasoDeMetropolis(Beta,ran64);	
	
	//tomo datos y evoluciono
	for(cmuestras=0;cmuestras<Nmuestras;cmuestras++){
		//tomas datos;
		M = Ising.GetM();
		E = Ising.GetE();
		Mprom+=M; M2prom+=M*M; Eprom+=E; E2prom+=E*E; M4prom+=M*M*M*M;
		//evoluciono hasta las situiente muestra, equivalente a tcorr pasos de Monte carlo
		for(t=0;t<tcorr;t++)
			for(mcs=0;mcs<L2;mcs++)
				Ising.UnPasoDeMetropolis(Beta,ran64);	
	}	

	
	//Proceso los datos

	Mprom /= Nmuestras; M2prom /= Nmuestras; M4prom /= Nmuestras; Eprom /= Nmuestras; E2prom /= Nmuestras;
	
	Cv = Beta*Beta*kB*(E2prom-Eprom*Eprom);
	Xs = Beta*(M2prom-Mprom*Mprom);
	Ub = 1.0-1.0/3.0*M4prom/(M2prom*M2prom);
	//IMprimo

	cout <<kT<<" "<<Eprom<<" "<<Mprom<<" "<<Cv<<" "<<Xs<<" "<<Ub<<endl;
	}
		
	
	//for(t=0; t<10; t++)
	//	cout<<ran64.r()<<endl;

	//Ising.Inicie();

	//cout<<Ising.GetE()<<endl;
	//cout<<Ising.GetM()<<endl;

return 0;


}
