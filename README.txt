Proyecto final
Tema: transiciones de fase de segunda especie y rompimiento espontáneo de simetría
#Se estudia un modelo de Ising modificado (interacción de varios vecinos, red con diferentes geometrías)
Autores:
Leonel Fernando Ardila (lfardilap@unal.edu.co)
Juan Sebastián Flórez (jsflorezj@unale.du.co)
Overleaf link:
https://www.overleaf.com/9751377gdgbgyjzcfvw
